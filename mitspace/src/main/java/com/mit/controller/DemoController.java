package com.mit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 
 */
@Controller
public class DemoController {

	@GetMapping("/")
	   public String w(Model model) {

	      return "website";
	   }
	
   @GetMapping("/admin")
   public String a(Model model) {

      return "template";
   }

}
